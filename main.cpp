#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>

using namespace std;

void rand_seq() {
    int N, d = RAND_MAX / 2;
    vector<int> seq;
    int mx = -RAND_MAX, mn = RAND_MAX;
    int sum_even = 0;

    cout << "Enter number of elements:" << endl;
    cin >> N;

    seq.resize(N);
    for (int i = 0; i < N; ++i) {
        seq[i] = rand() - d;
    }

    for (int i = 0; i < N; ++i) {
        mx = max(mx, seq[i]);
        mn = min(mn, seq[i]);
        if (seq[i] % 2 == 0) {
            sum_even += seq[i];
        }
    }

    cout << "Sequence:" << endl;
    for (int i = 0; i < N; ++i) {
        cout << seq[i] << " ";
    }
    cout << endl;
    cout << "Maximum of the sequence:" << endl;
    cout << mx << endl;
    cout << "Minimum of the sequence:" << endl;
    cout << mn << endl;
    cout << "Sum of even numbers in the sequence:" << endl;
    cout << sum_even << endl;
}

void poly() {
    int poly_power;
    vector<int> coef;
    vector<int> sum(3, 0);

    cout << "Enter polynomial's power:" << endl;
    cin >> poly_power;

    coef.resize(poly_power+1);

    for (int i = 0; i <= poly_power; ++i) {
        cout << "Enter coefficient " << i << ":" << endl;
        cin >> coef[i];
    }

    for (int i = 0; i <= poly_power; ++i) {
        for (int x = 0; x <= 2; ++x) {
            int cur_val = coef[i];

            for (int pow = 0; pow < i; ++pow) {
                cur_val *= x;
            }

            sum[x] += cur_val;
        }
    }

    for (int x = 0; x <= 2; ++x) {
        cout << "Sum of the polynomial at x = " << x << ": " << endl;
        cout << sum[x] << endl;
    }
}

int main() {
    srand(time(0));

    int part;

    cout << "Enter 1 for parts a, b, c and 2 for part d:" << endl;
    cin >> part;

    if (part == 1) {
        rand_seq();
    }
    else {
        poly();
    }
}
